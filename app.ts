import * as fs from 'fs';
import render from './src/render'

const SOURCE_FILE = './src/index.md';
const DIST_FILE = './dist/index.html';

// READ
const mdContent = fs.readFileSync(SOURCE_FILE,{encoding: "utf8"}); 

// RENDER
const htmlContent = render(mdContent);
console.log(htmlContent);

// WRITE
fs.writeFileSync(DIST_FILE, htmlContent);
